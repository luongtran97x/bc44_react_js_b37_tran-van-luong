import logo from './logo.svg';
import './App.css';
import Validation from './validationForm/Validation';

function App() {
  return (
   <Validation></Validation>
  );
}

export default App;
