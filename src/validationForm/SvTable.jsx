import React, { Component } from "react";
import { connect } from "react-redux";
import {
  handelDelAction,
  handelEditAction,
  handelSearchAction,
} from "./redux/action/actionValidate";

class SvTable extends Component {
  state = {
    search: "",
  };

  handelOnChange = (e) => {
    this.setState({
      search: e.target.value,
    });
  };
  renderItem = () => {
    const { data } = this.props;
    return data.map((item) => {
      return (
        <>
          <tr style={{ background: "pink", color: "#000" }} key={item.id}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.phone}</td>
            <td>{item.email}</td>
            <td>
              <button
                className="btn btn-danger mx-2"
                onClick={() => {
                  this.props.handelDel(item);
                }}
              >
                Xóa
              </button>
              <button
                className="btn btn-success"
                onClick={() => {
                  this.props.handelEdit(item);
                }}
              >
                Sửa
              </button>
            </td>
          </tr>
        </>
      );
    });
  };

  render() {
    return (
      <>
        <div className="searchBar d-flex mt-5 justify-content-end">
          <input
            type="text"
            className="form-control w-50 mx-2"
            onChange={this.handelOnChange}
            placeholder="Nhập id  sinh viên để tìm kiếm..."
          />
          <button
            className="btn btn-success w-10"
            onClick={() => {
              this.props.handelSearch(this.state.search)
            }
            }
          >
            <i class="fa fa-search mx-2"></i>
            Search
          </button>
        </div>
        <div className="mt-2  text-white text-center ">
          <table class="table ">
            <thead>
              <tr className="bg-dark">
                <th>Mã Sv</th>
                <th>Họ Tên</th>
                <th>Số Điện Thoại</th>
                <th>Email</th>
                <th>Thao Tác</th>
              </tr>
            </thead>
            <tbody>{this.renderItem()}</tbody>
          </table>
        </div>
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    data: state.validateProducer.svArr,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handelDel: (item) => {
      dispatch(handelDelAction(item));
    },
    handelEdit: (item) => {
      dispatch(handelEditAction(item));
    },
    handelSearch: (value) => {
      dispatch(handelSearchAction(value));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SvTable);
