import React, { Component } from "react";
import SvForm from "./SvForm";
import SvTable from "./SvTable";


export default class Validation extends Component {
  render() {
    return (
      <div style={{background:"linear-gradient(#80FFDB, #5390D9)",height:"100vh"}}>
          <div className="container">
            <h3 className="text-center text-white py-5 ">Quản Lý Sinh Viên</h3>
            <SvForm></SvForm>
            <SvTable></SvTable>
         
          </div>
      </div>
    );
  }
}
