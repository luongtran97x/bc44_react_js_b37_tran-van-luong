import React, { Component } from "react";
import { connect } from "react-redux";
import {
  handelCreateAction,
  handelUpdateAction,
} from "./redux/action/actionValidate";
import { Regex } from "./redux/constant/regex";
class SvForm extends Component {
  state = {
    values: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
    errors: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
  };

  formValid = () => {
    let inValid = true;
    Object.values(this.state.values).forEach((value) => {
      if (!value) {
        inValid = false;
      }
    });
    return inValid;
  };

  handelOnBlur = (event) => {
    const { value, name } = event.target;
    let cloneValues = { ...this.state.values, [name]: value };
    let cloneErrror = { ...this.state.errors };

    if (!value.trim()) {
      cloneErrror[name] = name + " Không được bỏ trống";
    } else {
      cloneErrror[name] = "";
    }
    this.setState({
      values: cloneValues,
      errors: cloneErrror,
    });
  };

  handelOnChange = (event) => {
    const { value, name, pattern, type } = event.target;
    let cloneValues = { ...this.state.values, [name]: value };
    let cloneErrror = { ...this.state.errors };

    if (!value.trim()) {
      cloneErrror[name] = name + " Không được bỏ trống";
    } else {
      if (pattern) {
        const regex = new RegExp(pattern);
        const inValid = regex.test(value);
        if (!inValid) {
          cloneErrror[name] = name + " Không đúng định dạng";
        } else {
          cloneErrror[name] = "";
        }
      }
    }
    if (type === "email") {
      const re = Regex.email;
     
      if (re.test(value)) {
        cloneErrror[name] = "";
      } else {
        cloneErrror[name] = name.toUpperCase() + " không hợp lệ";
      }
    }
    if (name === "phone") {
      const re = Regex.phone;
      if (re.test(value)) {
        cloneErrror[name] = "";
      } else {
        cloneErrror[name] = name.toUpperCase() + " không hợp lệ";
      }
    }
    this.setState({
      values: cloneValues,
      errors: cloneErrror,
    });
  };

  handelSubmit = (event) => {
    event.preventDefault();
  };

  render() {
    const { values } = this.state;
    const { errors } = this.state;
    return (
      <div>
        <form
          action=""
          style={{ background: "pink", borderRadius: "10px" }}
          onSubmit={this.handelSubmit}
        >
          <h4 className=" text-white p-3" style={{ background: "#242424" }}>
            Thông Tin Sinh Viên
          </h4>
          <div className="row">
            <div className="col-6">
              <label htmlFor="">Mã Sv</label>
              <input
                type="text"
                className="form-control"
                name="id"
                id="id"
                required
                pattern="^[0-9a-zA-Z]{1,5}$"
                value={values.id}
                onChange={this.handelOnChange}
                onBlur={this.handelOnBlur}
              />
              {errors.id && (
                <span className="text text-danger">{errors.id}</span>
              )}
            </div>
            <div className="col-6">
              <label htmlFor="">Họ Tên</label>
              <input
                type="text"
                className="form-control"
                name="name"
                id="name"
                required
                value={values.name}
                onBlur={this.handelOnBlur}
                onChange={this.handelOnChange}
              />
              <span className="text-danger">{errors.name}</span>
            </div>
            <div className="col-6 mt-2">
              <label htmlFor="">Số Điện Thoại</label>
              <input
                type="text"
                className="form-control "
                name="phone"
                value={values.phone}
                id="phone"
                required
                onBlur={this.handelOnBlur}
                onChange={this.handelOnChange}
              />
              <span className="text-danger">{errors.phone}</span>
            </div>
            <div className="col-6 mt-2">
              <label htmlFor="">Email</label>
              <input
                type="email"
                className="form-control "
                name="email"
                value={values.email}
                id="email"
                required
                onBlur={this.handelOnBlur}
                onChange={this.handelOnChange}
              />
              <span className="text-danger">{errors.email}</span>
            </div>
          </div>
          <div className="button p-2">
            <button
              className="btn btn-danger mt-2"
              onClick = {() => {
                this.props.handelCreate(this.state.values);
              }
              }
            >
              Thêm Sinh Viên
            </button>
            <button
              className="btn btn-success mt-2 mx-2"
              onClick={() => {
                this.props.handelUpdate(this.state.values);
              }}
            >
              Cập Nhật
            </button>
          </div>
        </form>
      </div>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.svEdit !== this.props.svEdit) {
      this.setState({
        values: this.props.svEdit,
      });
    }
  }
}
let mapStateToProps = (state) => {
  return {
    svEdit: state.validateProducer.svEdit,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handelCreate: (item) => dispatch(handelCreateAction(item)),
    handelUpdate: (item) => dispatch(handelUpdateAction(item)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SvForm);
