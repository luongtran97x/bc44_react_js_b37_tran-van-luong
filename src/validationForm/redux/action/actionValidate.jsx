import { add, del, edit, search, update } from "../constant/validateConstant";

export const handelCreateAction = (item) => {
  return {
    type: add,
    payload: item,
  };
};

export const handelDelAction = (item) => {
  return {
    type: del,
    payload: item,
  };
};
export const handelEditAction = (item) => {
  return {
    type: edit,
    payload: item,
  };
};

export const handelUpdateAction = (item) => {
  return {
    type: update,
    payload: item
  };
};
export const handelSearchAction =(value) => {
  return{
    type: search,
    payload:value
  }
}
