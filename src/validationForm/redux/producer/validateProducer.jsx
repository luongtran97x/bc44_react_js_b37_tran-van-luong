import { act } from "react-dom/test-utils";
import { add, del, edit, search, update } from "../constant/validateConstant";

let initialState = {
  svArr: [],
  svEdit: {},
  svSearch: { id: 1, name: "teo", phone: "00000", email: "dasdghja@gmail.com" },
};
export const validateProducer = (state = initialState, action) => {
  switch (action.type) {
    case add: {
      let cloneSvArr = [...state.svArr];
      let index = cloneSvArr.findIndex((sv) => sv.id === action.payload.id);
      if (index !== -1) {
        alert(" Id đã tồn tại");
      } else {
        cloneSvArr.push(action.payload);
      }
      return { ...state, svArr: cloneSvArr };
    }
    case del: {
      let cloneSvArr = state.svArr.filter((sv) => sv.id !== action.payload.id);
      return { ...state, svArr: cloneSvArr };
    }
    case edit: {
      document.getElementById("id").disabled = true;
      return { ...state, svEdit: action.payload };
    }
    case update: {
      document.getElementById("id").disabled = false;
      let cloneSvArr = [...state.svArr];
      let index = cloneSvArr.findIndex((sv) => sv.id === action.payload.id);
      if (index !== -1) {
        cloneSvArr[index] = action.payload;
      }
      return { ...state, svArr: cloneSvArr };
    }
    case search: {
      console.log("action", action.payload);
      let searchSv = state.svArr.filter((sv) => sv.id === action.payload);
      console.log("🚀 ~ searchSv:", searchSv);
      if (searchSv.length !== 0) {
        state.svArr = searchSv;
      }
      return { ...state};

      // let cloneSv = [...state.svArr];
      // let index = cloneSv.findIndex((sp) => sp.id === action.payload);
      // if (index != -1) {
      //   state.svArr = cloneSv[index];
      // }
      // return{...state}
    }
    default:
      return state;
  }
};
